# TrainHeroic

## Requirements to run the project:
-Ruby 2x
-Nodejs
-Typescript v2
-Sass
-Angular-CLI

Go to the folder [trainHeroic] and run from the console the follwoing command: ng-serve
It should start the local server and initialize the bundle on Webpack.

## Regarding the questions from the Challenge:

## -What are you calculating/displaying?
The json returned from the API comes with some information that is not to easy to interpretate, also I noticed that the rank and average rank had the same value on all the nodes, and that the json already comes sorted by rank.
So I am displaying the information I found most basic: Name, profile pic, rank. At the top of the page I am displaying the name of the excercise.

## -How many results are you going to display at a time? (N) Obviously you won’t be able to view 300 athletes at once on a TV, so what number
did you cut that down to and why?
Since we are working with a tv screen I am assuming a fixed resolution of 1280px x 800px. No need for responsive design. I choose to display 16 results for page, since the information doesn't occupy to much space and given the time each person can find its results without waiting too much.

## -What animations are you using to replace those results with the next N in the list? We still want to view all of those 300 athletes, how quickly
are you cycling through them?
It is my first time working with material design. One thing I noticed is that the "md-grid-tile" component calculates the position of the element and then sets a fixed top and left values using position absolute. This was giving me issues at the time of using ngClass to add a "hidden" class, because even if the elements got hidden, the position of the following elements was staying at the same value.
So instead of using ngClass I opted to use a ngIf for this excercise. This comes with the problem that ngIf removes the element completely, so I can't really add a css class to animate the remove effect. Instead I am adding a class to fade in the active elements using pure css3.
Also, I am cycling each page at a speed of 15 seconds. I tested myself reading all the names and still had some few seconds left.

## -What user experience considerations did you make with your solution / tradeoffs?
I consider the following things:
What information was pertinent to show without making anyone uncomfortable or breaching privacy? For example, avoid adding the username since it could compromise access to the user account.
How to help users identify themselves faster? That is why I added the profile pic.
Since it is a sport app, competitive environment is something that can result attractive to people, beside helping them to track their progress, so I added that also.

Since there is no interaction of the user, the following most important aspects where to allow them to find their names without waiting to much, and giving them time to read all the page info withouth missing anything. In this aspect I believe that the spacing and amount of data also helps readibility of the information. Of course, a designer could improve my work here.


## -What third party / open source libs did you use and why?
For this project I am using angular2 with material design.
For generating the angular2 project I am using Angular-CLI because it allows me to generate the project scaffolding and differents components following the guidelines of the framework and reducing the space for errors.
I needed to install the material design dependency in order to being able to use the components like cards and being able to offer a look and feel similar to the one of the provided page.
Besides that, the project already comes configured to use typescript and scss.


This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.19-3.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Deploying to Github Pages

Run `ng github-pages:deploy` to deploy to Github Pages.

## Further help

To get more help on the `angular-cli` use `ng --help` or go check out the [Angular-CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
