import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable }     from 'rxjs/Observable';
import 'rxjs/Rx'; //Fix for error with map, catch and other functions not being in typings for observables.

@Injectable()
export class ServiceWorkoutResultsService {
	private workoutUrl = 'https://apis.trainheroic.com/public/leaderboard/468425';  // URL to web API

  constructor(private http: Http) { }

	getWorkoutResults(): Observable<Workout[]> {
    return this.http.get(this.workoutUrl)
										.map((res:Response) => res.json())
                    .catch(this.handleError);
  }


  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}

export class Workout {
  results: Object;
  title: string;
  constructor(obj?: any) {
  	console.log(obj);
    this.results = obj && obj.results || null;
    this.title = obj && obj.tests || null;
  }
}
