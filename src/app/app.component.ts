import { Component } from '@angular/core';
import { ServiceWorkoutResultsService } from './shared/index';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  viewProviders: [ServiceWorkoutResultsService]
})
export class AppComponent {
  title = 'app works!';
}
