/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ServiceWorkoutResultsService } from './service-workout-results.service';

describe('Service: ServiceWorkoutResults', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ServiceWorkoutResultsService]
    });
  });

  it('should ...', inject([ServiceWorkoutResultsService], (service: ServiceWorkoutResultsService) => {
    expect(service).toBeTruthy();
  }));
});
