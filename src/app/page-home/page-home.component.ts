import { Component, OnInit } from '@angular/core';
import { Workout, ServiceWorkoutResultsService } from '../shared/index';
import {Observable} from 'rxjs/Rx';

@Component({
  selector: 'app-page-home',
  templateUrl: './page-home.component.html',
  styleUrls: ['./page-home.component.scss']
})
export class PageHomeComponent implements OnInit {
  currentIndex: number;
  itemsPerPage: number;
  resultsLength: number;
  timeoutValue: number;
  workoutResults: Array<Workout>;

  constructor(private serviceWorkoutResultsService: ServiceWorkoutResultsService) { }

  ngOnInit() {
		this.currentIndex = 0;
  	this.itemsPerPage = 16;
    this.timeoutValue = 15000;
  	this.getWorkoutResults();
  }

  checkItemVisibility( itemIndex ): boolean {
  	let result = false;
  	if( itemIndex >= (this.currentIndex) && itemIndex < (this.currentIndex + 16) ){
  		result = true;
  	}
  	return result;
  }

  getWorkoutResults(): void {
	  this.serviceWorkoutResultsService.getWorkoutResults().subscribe(
	    data => { 
	    	this.workoutResults = data; 
  			this.resultsLength = this.workoutResults['results'].length;
		  	this.manageTimer();
	    },
	    error => console.log(error)
	  );
	}

	manageTimer(): void {
  	let timer = Observable.timer(this.timeoutValue,this.timeoutValue);
    timer.subscribe(
    	t=> {
    		this.currentIndex += this.itemsPerPage;
    		if( this.currentIndex > this.resultsLength ) {
		  		this.currentIndex = 0;
		  	}
    	}
  	);
	}
}