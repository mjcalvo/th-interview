import { NgModule }     from '@angular/core';
import { RouterModule } from '@angular/router';

import { PageHomeComponent } from './page-home/page-home.component';

@NgModule({
  imports: [
    RouterModule.forRoot([
      { path: '', component: PageHomeComponent },
      { path: '**', component: PageHomeComponent }
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}