import { TrainHeroicPage } from './app.po';

describe('train-heroic App', function() {
  let page: TrainHeroicPage;

  beforeEach(() => {
    page = new TrainHeroicPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
